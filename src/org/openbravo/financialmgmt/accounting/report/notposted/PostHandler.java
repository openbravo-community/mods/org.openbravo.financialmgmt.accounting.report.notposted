/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2013-2017 Openbravo SLU
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package org.openbravo.financialmgmt.accounting.report.notposted;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.model.ModelProvider;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.client.kernel.BaseActionHandler;
import org.openbravo.client.kernel.RequestContext;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.ad_forms.AcctServer;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.model.ad.datamodel.Table;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.service.db.DalConnectionProvider;
import org.openbravo.service.db.DbUtility;

public class PostHandler extends BaseActionHandler {
  private static final Logger log4j = Logger.getLogger(PostHandler.class);

  @Override
  protected JSONObject execute(Map<String, Object> parameters, String content) {
    JSONObject response = new JSONObject();
    try {
      final JSONObject request = new JSONObject(content);
      OBError myError = null;
      // final String action = request.getString("action");
      final JSONArray documentIdList = request.getJSONArray("recordIdList");
      VariablesSecureApp vars = RequestContext.get().getVariablesSecureApp();
      if (documentIdList.length() == 0) {
        throw new OBException("@NotSelected@");
      }
      List<String> ids = parseJSON(documentIdList);
      int errors = 0;
      int oks = 0;
      for (String id : ids) {
        String tableId = id.substring(0, id.indexOf("-"));
        String recordId = id.substring(id.indexOf("-") + 1);
        OBError error = post(tableId, recordId);
        if (error != null) {
          errors = errors + 1;
        } else {
          oks = oks + 1;
        }
        if (ids.size() == 1) {
          myError = error;
        }
      }
      if (myError == null) {
        JSONObject errorMessage = new JSONObject();
        errorMessage.put("severity", "success");
        errorMessage.put("text",
            Utility.messageBD(new DalConnectionProvider(false), "ProcessOK", vars.getLanguage())
                + "\n Posted Documents:" + oks + "\n Errors:" + errors);
        response.put("message", errorMessage);
      } else {
        JSONObject errorMessage = new JSONObject();
        errorMessage.put("severity", myError.getType().toLowerCase());
        errorMessage.put(
            "text",
            Utility.messageBD(new DalConnectionProvider(false), myError.getMessage(),
                vars.getLanguage()));
        response.put("message", errorMessage);
      }
    } catch (Exception e) {
      OBDal.getInstance().rollbackAndClose();
      log4j.error("Post error: " + e.getMessage(), e);

      Throwable ex = DbUtility.getUnderlyingSQLException(e);
      String message = OBMessageUtils.translateError(ex.getMessage()).getMessage();
      try {
        JSONObject errorMessage = new JSONObject();
        errorMessage.put("severity", "error");
        errorMessage.put("text", message);
        response.put("message", errorMessage);
      } catch (JSONException ignore) {
      }
    }
    return response;
  }

  private OBError post(String tableId, String recordId) {
    if (log4j.isDebugEnabled())
      log4j.debug("ProcessButton strKey: " + recordId + "strTableId: " + tableId);
    String strOrg;
    Connection con = null;
    OBError myMessage = null;
    VariablesSecureApp vars = new VariablesSecureApp(OBContext.getOBContext().getUser().getId(),
        OBContext.getOBContext().getCurrentClient().getId(), OBContext.getOBContext()
            .getCurrentOrganization().getId(), OBContext.getOBContext().getLanguage().getLanguage());
    DalConnectionProvider cp = new DalConnectionProvider(false);
    Table table = OBDal.getInstance().get(Table.class, tableId);
    BaseOBObject ob = OBDal.getInstance().get(
        ModelProvider.getInstance().getEntityByTableName(table.getDBTableName()).getName(),
        recordId);
    strOrg = ((Organization) ob.get("organization")).getId();
    if (strOrg == null)
      strOrg = "0";
    try {
      con = cp.getTransactionConnection();
      AcctServer acct = AcctServer.get(tableId,
          OBContext.getOBContext().getCurrentClient().getId(), strOrg, cp);
      if (acct == null) {
        cp.releaseRollbackConnection(con);
        myMessage = Utility.translateError(cp, vars, OBContext.getOBContext().getLanguage()
            .getLanguage(), "ProcessRunError");
        return myMessage;
      } else if (!acct.post(recordId, false, vars, cp, con) || acct.errors != 0) {
        cp.releaseRollbackConnection(con);
        myMessage = acct.getMessageResult();
        return myMessage;
      }
      cp.releaseCommitConnection(con);
    } catch (Exception e) {
      log4j.error(e);
      myMessage = Utility.translateError(cp, vars, OBContext.getOBContext().getLanguage()
          .getLanguage(), e.getMessage());
      try {
        cp.releaseRollbackConnection(con);
      } catch (Exception ignored) {
      }
    } finally {
      OBDal.getInstance().commitAndClose();
    }
    return myMessage;
  }

  public static List<String> parseJSON(JSONArray idJSON) throws JSONException {
    List<String> ids = new ArrayList<String>();
    for (int i = 0; i < idJSON.length(); i++) {
      ids.add(idJSON.getString(i));
    }
    return ids;

  }
}