/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2011-2018 Openbravo SLU
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package org.openbravo.financialmgmt.accounting.report.notposted;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;
import org.openbravo.base.model.Entity;
import org.openbravo.base.model.ModelProvider;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.client.kernel.RequestContext;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.model.ad.datamodel.Table;
import org.openbravo.model.common.enterprise.DocumentType;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.enterprise.OrganizationAcctSchema;
import org.openbravo.model.financialmgmt.accounting.coa.AcctSchema;
import org.openbravo.model.financialmgmt.accounting.coa.AcctSchemaTable;
import org.openbravo.service.datasource.ReadOnlyDataSourceService;
import org.openbravo.service.json.JsonConstants;
import org.openbravo.service.json.JsonUtils;

public class NotPostedDocumentsDataSource extends ReadOnlyDataSourceService {

  private static Logger log4j = LogManager.getLogger();
  private final SimpleDateFormat xmlDateFormat = JsonUtils.createDateFormat();
  private final SimpleDateFormat xmlDateTimeFormat = JsonUtils.createDateTimeFormat();
  private static final String AD_TABLE_ID = "429F3E2A084A48A0B0B2656A20B32A68";
  private static final int RECORDSLIMIT = 1000;

  @Override
  public Entity getEntity() {
    return ModelProvider.getInstance().getEntityByTableId(AD_TABLE_ID);
  }

  @Override
  protected int getCount(Map<String, String> parameters) {
    return -1;
  }

  public String fetch(Map<String, String> parameters) {
    String preferenceValue = Utility.getPreference(RequestContext.get().getVariablesSecureApp(),
        "OBNPT_NotPostedReportLimit", "");
    int numPerformanceLimit = StringUtils.isNotEmpty(preferenceValue) ? Integer
        .parseInt(preferenceValue) : RECORDSLIMIT;
    int startRow = Integer.parseInt(parameters.get(JsonConstants.STARTROW_PARAMETER));
    if (startRow >= numPerformanceLimit) {
      Utility.throwErrorMessage("ReportsLimitHeader");
    }
    parameters.put(JsonConstants.NOCOUNT_PARAMETER, "false");
    return super.fetch(parameters);
  }

  @Override
  protected List<Map<String, Object>> getData(Map<String, String> parameters, int startRow,
      int endRow) {
    List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
    if (parameters.get(JsonConstants.DISTINCT_PARAMETER) != null) {
      String distinct = (String) parameters.get(JsonConstants.DISTINCT_PARAMETER);
      log4j.debug("Distinct param: {}", distinct);
      if ("organization".equals(distinct)) {
        result = getOrganizationFilterData(parameters);
      } else if ("documentType".equals(distinct)) {
        result = getDocumentTypeFilterData(parameters, startRow, endRow);
      }
    } else {
      result = getGridData(parameters, startRow, endRow);
      log4j.debug("data length: {}", result.size());
    }
    return result;
  }

  private List<Map<String, Object>> getOrganizationFilterData(Map<String, String> parameters) {
    List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
    Map<String, String> filterCriteria = new HashMap<String, String>();
    try {
      // Builds the criteria based on the fetch parameters
      JSONArray criterias = (JSONArray) JsonUtils.buildCriteria(parameters).get("criteria");
      for (int i = 0; i < criterias.length(); i++) {
        final JSONObject criteria = criterias.getJSONObject(i);
        filterCriteria.put(criteria.getString("fieldName"), criteria.getString("value"));
      }
    } catch (JSONException e) {
      log4j.error("Error while building the criteria", e);
    }
    OBContext.setAdminMode();
    try {
      for (Organization o : getFilteredOrganizations(filterCriteria.get("organization$_identifier"))) {
        Map<String, Object> myMap = new HashMap<String, Object>();
        myMap.put("id", o.getId());
        myMap.put("name", o.getIdentifier());
        myMap.put("_identifier", o.getIdentifier());
        myMap.put("_entityName", "Organization");
        result.add(myMap);
      }
    } finally {
      OBContext.restorePreviousMode();
    }
    return result;
  }

  private List<Map<String, Object>> getDocumentTypeFilterData(Map<String, String> parameters,
      int startRow, int endRow) {
    List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
    Map<String, String> filterCriteria = new HashMap<String, String>();
    try {
      // Builds the criteria based on the fetch parameters
      JSONArray criterias = (JSONArray) JsonUtils.buildCriteria(parameters).get("criteria");
      for (int i = 0; i < criterias.length(); i++) {
        final JSONObject criteria = criterias.getJSONObject(i);
        filterCriteria.put(criteria.getString("fieldName"), criteria.getString("value"));
      }
    } catch (JSONException e) {
      log4j.error("Error while building the criteria", e);
    }
    OBContext.setAdminMode();
    ScrollableResults scroller = null;
    try {
      scroller = getFilteredDocumentTypes(filterCriteria.get("documentType$_identifier"), startRow,
          endRow);
      while (scroller.next()) {
        DocumentType doctype = getDocumentType(scroller.get()[0].toString());
        Map<String, Object> myMap = new HashMap<String, Object>();
        myMap.put("id", doctype.getId());
        myMap.put("name", doctype.getName());
        myMap.put("_identifier", doctype.getIdentifier());
        myMap.put("_entityName", "DocumentType");
        result.add(myMap);
      }
    } finally {
      OBContext.restorePreviousMode();
      if (scroller != null) {
        scroller.close();
      }
    }
    return result;
  }

  private ScrollableResults getFilteredDocumentTypes(String contains, int startRow, int endRow) {
    StringBuilder hql = new StringBuilder();
    hql.append("select distinct (e.name) from DocumentType e where e.client.id = :clientId ");
    if (contains != null && !"".equals(contains)) {
      if (contains.startsWith("[")) {
        try {
          JSONArray myJSON = new JSONArray(contains);
          for (int i = 0; i < myJSON.length(); i++) {
            JSONObject myJSONObject = (JSONObject) myJSON.get(i);
            hql.append((i == 0) ? " and " : " or ");
            hql.append(" upper(e.name) like upper('%").append(myJSONObject.get("value"))
                .append("%')");
          }
        } catch (JSONException e) {
          log4j.error("Error getting filter document types", e);
        }
      } else {
        hql.append(" and upper (e.name) like upper('%").append(contains).append("%')");
      }
    }
    hql.append(" order by e.name");

    @SuppressWarnings("unchecked")
    Query<Object> qry = OBDal.getInstance().getSession().createQuery(hql.toString());
    qry.setParameter("clientId", OBContext.getOBContext().getCurrentClient().getId());
    if (startRow != -1 && endRow != -1) {
      qry.setFirstResult(startRow);
      qry.setMaxResults(endRow - startRow);
      qry.setFetchSize(endRow - startRow);
    }
    return qry.scroll(ScrollMode.FORWARD_ONLY);
  }

  private List<Organization> getFilteredOrganizations(String contains) {
    OBCriteria<Organization> obc = OBDal.getInstance().createCriteria(Organization.class);
    obc.add(Restrictions.eq(AcctSchemaTable.PROPERTY_CLIENT, OBContext.getOBContext()
        .getCurrentClient()));
    obc.add(Restrictions.in(Organization.PROPERTY_ID,
        Arrays.asList(OBContext.getOBContext().getReadableOrganizations())));
    obc.setFilterOnReadableClients(false);
    obc.setFilterOnReadableOrganization(false);
    obc.setFilterOnActive(true);
    if (StringUtils.isNotEmpty(contains)) {
      if (contains.startsWith("[")) {
        try {
          JSONArray myJSON = new JSONArray(contains);
          Criterion myCriterion = null;
          for (int i = 0; i < myJSON.length(); i++) {
            JSONObject myJSONObject = (JSONObject) myJSON.get(i);
            if (myCriterion == null) {
              myCriterion = Restrictions.ilike(Organization.PROPERTY_NAME,
                  "%" + myJSONObject.get("value") + "%");
            } else {
              myCriterion = Restrictions.or(
                  myCriterion,
                  Restrictions.ilike(Organization.PROPERTY_NAME, "%" + myJSONObject.get("value")
                      + "%"));
            }
          }
          obc.add(myCriterion);
        } catch (JSONException e) {
          log4j.error("Error getting filter organizations", e);
        }
      } else {
        obc.add(Restrictions.ilike(Organization.PROPERTY_NAME, "%" + contains + "%"));
      }
    }
    return obc.list();
  }

  private List<Map<String, Object>> getGridData(Map<String, String> parameters, int startRow,
      int endRow) {
    int counter = 0;
    List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
    // To Avoid Duplicates but maintain order
    Set<Map<String, Object>> resultSet = new HashSet<Map<String, Object>>();
    Map<String, String> filterCriteria = new HashMap<String, String>();
    try {
      // Builds the criteria based on the fetch parameters
      JSONArray criterias = (JSONArray) JsonUtils.buildCriteria(parameters).get("criteria");
      for (int i = 0; i < criterias.length(); i++) {
        final JSONObject criteria = criterias.getJSONObject(i);
        // Multiple selection
        if (criteria.has("criteria") && criteria.has("operator")) {
          JSONArray mySon = new JSONArray(criteria.getString("criteria"));
          filterCriteria.put(mySon.getJSONObject(0).getString("fieldName"),
              criteria.getString("criteria"));
        } else if (criteria.has("operator")
            && ("greaterOrEqual".equals(criteria.getString("operator")) || "lessOrEqual"
                .equals(criteria.getString("operator")))) {
          filterCriteria.put(
              criteria.getString("operator") + "_" + criteria.getString("fieldName"),
              criteria.getString("value"));
        } else {
          filterCriteria.put(criteria.getString("fieldName"), criteria.getString("value"));
        }
      }
    } catch (JSONException e) {
      log4j.error("Error while building the criteria", e);
    }
    OBContext.setAdminMode(true);
    ScrollableResults scroller = null;
    ScrollableResults scrollerDocTypes = null;
    try {
      OBCriteria<AcctSchemaTable> obc = OBDal.getInstance().createCriteria(AcctSchemaTable.class);
      obc.add(Restrictions.eq(AcctSchemaTable.PROPERTY_CLIENT, OBContext.getOBContext()
          .getCurrentClient()));
      obc.setFilterOnReadableClients(false);
      obc.setFilterOnReadableOrganization(false);
      obc.setFilterOnActive(true);
      String contains = "";
      if (filterCriteria.get("organization$_identifier") != null) {
        contains = filterCriteria.get("organization$_identifier");
      }
      int j = 0;
      List<AcctSchemaTable> schemaTables = obc.list();
      for (AcctSchemaTable schemaTable : schemaTables) {
        Set<Organization> orgs = getOrganizations(schemaTable.getAccountingSchema(),
            getFilteredOrganizations(contains));
        if (orgs.isEmpty()) {
          continue;
        }
        Table table = OBDal.getInstance().get(AcctSchemaTable.class, schemaTable.getId())
            .getTable();
        Entity entityByTableName = ModelProvider.getInstance().getEntityByTableName(
            table.getDBTableName());
        String tableDate = entityByTableName.getPropertyByColumnName(
            table.getAcctdateColumn().getDBColumnName()).getName();
        final StringBuilder hqlString = new StringBuilder();
        hqlString.append("select e from ").append(entityByTableName.getName()).append(" e ");
        hqlString
            .append(" where e.posted not in ('Y','D') and e.processed = 'Y' and e.organization in (:orgs)");
        if ("FIN_Payment".equals(table.getDBTableName())) {
          hqlString.append(" and e.status <> 'RPVOID' ");
        }
        Date exactDate = null;
        Date dateFrom = null;
        Date dateTo = null;
        String date = "";
        String operator = "=";
        try {
          if (filterCriteria.get("accountingDate") != null) {
            date = filterCriteria.get("accountingDate");
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            exactDate = sdf.parse(date);
            if (exactDate != null) {
              hqlString.append(" and e.").append(tableDate).append(operator).append(" :date ");
            }
          }
          if (filterCriteria.get("greaterOrEqual_accountingDate") != null) {
            date = filterCriteria.get("greaterOrEqual_accountingDate");
            operator = ">=";
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            dateFrom = sdf.parse(date);
            if (dateFrom != null) {
              hqlString.append(" and e.").append(tableDate).append(operator).append(" :dateFrom ");
            }
          }
          if (filterCriteria.get("lessOrEqual_accountingDate") != null) {
            date = filterCriteria.get("lessOrEqual_accountingDate");
            operator = "<=";
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            dateTo = sdf.parse(date);
            if (dateTo != null) {
              hqlString.append(" and e.").append(tableDate).append(operator).append(" :dateTo ");
            }
          }
        } catch (ParseException e) {
          log4j.error("could not parse date: {}", date, e);
        }
        List<String> postedList = new ArrayList<String>();
        if (filterCriteria.get("posted") != null) {
          hqlString.append(" and e.posted in :postedList ");
          try {
            JSONArray myJsonArray = new JSONArray(filterCriteria.get("posted"));
            for (int i = 0; i < myJsonArray.length(); i++) {
              postedList.add((String) myJsonArray.get(i));
            }
          } catch (JSONException e) {
            log4j.error("Error parsing posted filer: {}", filterCriteria.get("posted"), e);
          }
        }
        boolean entityHasDocumentTypeProperty = entityByTableName.hasProperty("documentType");
        DocumentType defaultDocumentTypeForTable = (!entityHasDocumentTypeProperty) ? getDefaultDocumentType(table)
            : null;
        if (entityHasDocumentTypeProperty) {
          hqlString.append(" order by e.documentType, e.organization, e.").append(tableDate);
        } else {
          hqlString.append(" order by e.organization, e.").append(tableDate);
        }
        final Session session = OBDal.getInstance().getSession();
        @SuppressWarnings("rawtypes")
        Query query = session.createQuery(hqlString.toString());
        query.setParameterList("orgs", orgs);
        if (exactDate != null) {
          query.setParameter("date", exactDate);
        }
        if (dateFrom != null) {
          query.setParameter("dateFrom", dateFrom);
        }
        if (dateTo != null) {
          query.setParameter("dateTo", dateTo);
        }
        if (!postedList.isEmpty()) {
          query.setParameterList("postedList", postedList);
        }
        List<DocumentType> documentTypes = new ArrayList<DocumentType>();
        if (filterCriteria.get("documentType$_identifier") != null) {
          scrollerDocTypes = getFilteredDocumentTypes(
              filterCriteria.get("documentType$_identifier"), startRow, endRow);
          while (scrollerDocTypes.next()) {
            documentTypes.add(getDocumentType(scrollerDocTypes.get()[0].toString()));
          }

        }
        if (endRow != -1) {
          query.setMaxResults(endRow);
        }
        scroller = query.scroll(ScrollMode.FORWARD_ONLY);
        while (scroller.next()) {
          Object o = (Object) scroller.get()[0];
          if (filterCriteria.get("document") != null
              && ((BaseOBObject) o).getIdentifier().toLowerCase()
                  .indexOf(filterCriteria.get("document").toLowerCase()) < 0) {
            continue;
          }
          Map<String, Object> myMap = new HashMap<String, Object>();
          if (entityHasDocumentTypeProperty) {
            myMap.put("documentType",
                ((BaseOBObject) ((BaseOBObject) o).get("documentType")).getId());
            myMap.put("documentType$_identifier",
                ((BaseOBObject) ((BaseOBObject) o).get("documentType")).getIdentifier());
          } else {
            // If no document type property exists then retrieve default document type for selected
            // table
            DocumentType docType = defaultDocumentTypeForTable;
            if (docType != null) {
              myMap.put("documentType", docType.getId());
              myMap.put("documentType$_identifier", docType.getIdentifier());
            } else {
              myMap.put("documentType", "");
              myMap.put("documentType$_identifier", "");
              log4j.warn("No document Type defined for table {}", table.getDBTableName());
            }
          }
          if (!documentTypes.isEmpty()
              && !documentTypes.contains(OBDal.getInstance().get(DocumentType.class,
                  myMap.get("documentType")))) {
            continue;
          }
          myMap.put("id", table.getId() + "-" + ((BaseOBObject) o).getId());
          myMap.put("client", ((BaseOBObject) ((BaseOBObject) o).get("client")).getId());
          myMap.put("client$_identifier",
              ((BaseOBObject) ((BaseOBObject) o).get("client")).getIdentifier());
          myMap.put("creationDate",
              xmlDateTimeFormat.format((Date) ((BaseOBObject) o).get("creationDate")));
          myMap.put("createdBy", ((BaseOBObject) ((BaseOBObject) o).get("createdBy")).getId());
          myMap.put("createdBy$_identifier",
              ((BaseOBObject) ((BaseOBObject) o).get("createdBy")).getIdentifier());
          myMap.put("updated", xmlDateTimeFormat.format((Date) ((BaseOBObject) o).get("updated")));
          myMap.put("updatedBy", ((BaseOBObject) ((BaseOBObject) o).get("updatedBy")).getId());
          myMap.put("updatedBy$_identifier",
              ((BaseOBObject) ((BaseOBObject) o).get("updatedBy")).getIdentifier());
          myMap.put("document", ((BaseOBObject) o).getIdentifier());
          myMap.put("table", table.getId());
          myMap.put("table$_identifier", table.getIdentifier());
          myMap.put("accountingDate",
              xmlDateFormat.format((Date) ((BaseOBObject) o).get(tableDate)));
          myMap
              .put("organization", ((BaseOBObject) ((BaseOBObject) o).get("organization")).getId());
          myMap.put("organization$_identifier",
              ((BaseOBObject) ((BaseOBObject) o).get("organization")).getIdentifier());
          myMap.put("posted", ((BaseOBObject) o).get("posted"));
          myMap.put("post", false);
          if (!resultSet.contains(myMap)) {
            if (startRow != -1 && endRow != -1) {
              if (counter >= startRow && counter < endRow) {
                result.add(myMap);
              }
              // Always add to the resultSet the current doc to avoid repetitions
              resultSet.add(myMap);
              if (counter >= endRow) {
                return result;
              }
              counter = counter + 1;
            } else {
              resultSet.add(myMap);
              result.add(myMap);
            }
          }
          OBDal.getInstance().getSession().evict(o);
          OBDal.getInstance().getSession().evict(schemaTable);
          if ((++j % 100) == 0) {
            OBDal.getInstance().getSession().clear();
          }
        }
      }
    } finally {
      OBContext.restorePreviousMode();
      if (scroller != null) {
        scroller.close();
      }
      if (scrollerDocTypes != null) {
        scrollerDocTypes.close();
      }
    }
    return result;
  }

  private DocumentType getDefaultDocumentType(Table table) {
    OBCriteria<DocumentType> obc = OBDal.getInstance().createCriteria(DocumentType.class);
    obc.add(Restrictions.eq(DocumentType.PROPERTY_TABLE, table));
    obc.setFilterOnReadableOrganization(false);
    obc.setMaxResults(1);
    return (DocumentType) obc.uniqueResult();
  }

  private DocumentType getDocumentType(String name) {
    OBCriteria<DocumentType> obc = OBDal.getInstance().createCriteria(DocumentType.class);
    obc.add(Restrictions.ilike(DocumentType.PROPERTY_NAME, name));
    obc.setFilterOnReadableOrganization(false);
    obc.setFilterOnActive(false);
    obc.setMaxResults(1);
    return (DocumentType) obc.uniqueResult();
  }

  private Set<Organization> getOrganizations(AcctSchema acctSchema, List<Organization> included) {
    Set<Organization> result = new HashSet<Organization>();
    List<String> readableOrgs = Arrays.asList(OBContext.getOBContext().getReadableOrganizations());
    OBContext.setAdminMode();
    try {
      OBCriteria<OrganizationAcctSchema> obc = OBDal.getInstance().createCriteria(
          OrganizationAcctSchema.class);
      obc.add(Restrictions.eq(OrganizationAcctSchema.PROPERTY_ACCOUNTINGSCHEMA, acctSchema));
      for (OrganizationAcctSchema organizationAcctSchema : obc.list()) {
        for (String orgId : OBContext.getOBContext().getOrganizationStructureProvider()
            .getChildTree(organizationAcctSchema.getOrganization().getId(), true)) {
          Organization org = OBDal.getInstance().get(Organization.class, orgId);
          if (included.contains(org) && readableOrgs.contains(org.getId())) {
            result.add(org);
          }
        }
      }
    } finally {
      OBContext.restorePreviousMode();
    }
    return result;
  }
}
