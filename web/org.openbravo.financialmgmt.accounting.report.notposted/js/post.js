/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2013-2019 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
isc.defineClass('Post_Field', isc.Label);

isc.Post_Field.addProperties({
  height: 1,
  width: 100,
  initWidget: function() {
    this.Super('initWidget', arguments);
  }
});

OB = OB || {};

OB.Post = {
  execute: function(params, view) {
    var i,
      selection = params.button.contextView.viewGrid.getSelectedRecords(),
      recordIdList = [];

    for (i = 0; i < selection.length; i++) {
      recordIdList.push(selection[i].id);
    }

    isc.PostProcessPopup.create({
      recordIdList: recordIdList,
      view: view,
      params: params
    }).show();
  },

  Post: function(params, view) {
    params.actionHandler =
      'org.openbravo.financialmgmt.accounting.report.notposted.PostHandler';
    params.adTabId = view.activeView.tabId;
    params.processId = 'idhere';
    OB.Post.execute(params, view);
  }
};

isc.defineClass('PostProcessPopup', isc.OBPopup);

isc.PostProcessPopup.addProperties({
  width: 320,
  height: 200,
  title: null,
  showMinimizeButton: false,
  showMaximizeButton: false,

  //Form
  mainform: null,

  //Button
  okButton: null,
  cancelButton: null,

  initWidget: function() {
    OB.TestRegistry.register(
      'org.openbravo.client.application.Post.popup',
      this
    );

    var recordIdList = this.recordIdList,
      params = this.params;

    this.okButton = isc.OBFormButton.create({
      title: OB.I18N.getLabel('OK'),
      popup: this,
      action: function() {
        var callback;

        callback = function(rpcResponse, data, rpcRequest) {
          var view = rpcRequest.clientContext.originalView.getView(
            params.adTabId
          );
          if (data.message) {
            view.messageBar.setMessage(
              data.message.severity,
              null,
              data.message.text
            );
          }

          rpcRequest.clientContext.popup.closeClick();
          rpcRequest.clientContext.originalView.refresh(false, false);
        };

        OB.RemoteCallManager.call(
          params.actionHandler,
          {
            recordIdList: recordIdList
          },
          {},
          callback,
          {
            originalView: this.popup.view,
            popup: this.popup
          }
        );
      }
    });

    OB.TestRegistry.register(
      'org.openbravo.client.application.Post.popup.okButton',
      this.okButton
    );

    this.cancelButton = isc.OBFormButton.create({
      title: OB.I18N.getLabel('Cancel'),
      popup: this,
      action: function() {
        this.popup.closeClick();
      }
    });

    this.items = [
      isc.VLayout.create({
        defaultLayoutAlign: 'center',
        align: 'center',
        width: '100%',
        layoutMargin: 10,
        membersMargin: 6,
        members: [
          isc.HLayout.create({
            defaultLayoutAlign: 'center',
            align: 'center',
            layoutMargin: 30,
            membersMargin: 6,
            members: this.mainform
          }),
          isc.HLayout.create({
            defaultLayoutAlign: 'center',
            align: 'center',
            membersMargin: 10,
            members: [this.okButton, this.cancelButton]
          })
        ]
      })
    ];

    this.Super('initWidget', arguments);
  }
});
